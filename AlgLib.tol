//////////////////////////////////////////////////////////////////////////////
// FILE   : AlgLib.tol
// PURPOSE: ALGLIB is a cross-platform numerical analysis and data 
//          processing library.
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
NameBlock AlgLib =
//////////////////////////////////////////////////////////////////////////////
[[
  //read only autodoc
  Text _.autodoc.name = "AlgLib";
  Text _.autodoc.brief =
    "API to ALGLIB numerical analysis library";
  Text _.autodoc.description = 
    "ALGLIB is a cross-platform numerical analysis and data processing library.\n"
    "M�s detalles en la"
    "<a href=\"https://www.tol-project.org/wiki/"
    "OfficialTolArchiveNetworkAlgLib\" target=\"_blank\"> "
    "p�gina wiki</a> del paquete AlgLib";
  Text _.autodoc.url = 
    "http://packages.tol-project.org/OfficialTolArchiveNetwork/repository.php";
  Set _.autodoc.keys = [["numerical analysis"]];
  Set _.autodoc.authors = [["vdebuen@tol-project.org"]];
  Text _.autodoc.minTolVersion = "v3.1";
  Text _.autodoc.platform = 
    TolPackage::TolPlatform::ObtainPlatform("OWN", "OWN", "OWN");
  Text _.autodoc.variant = 
    TolPackage::TolPlatform::ObtainVariant(_.autodoc.platform);
  Real _.autodoc.version.high = 3;
  Real _.autodoc.version.low = 702;
  Set _.autodoc.dependencies = Copy(Empty);
  Set _.autodoc.nonTolResources = [[
    TolPackage::TolPlatform::ObtainDllFolder(?)
  ]];
  Text _.autodoc.versionControl = AvoidErr.NonDecAct(OSSvnInfo("."));
  
  NameBlock CppTools = [[ Real unused = ?]];
  
  Real _is_started = False;
  
  Text _dllFile = "";
  
  ////////////////////////////////////////////////////////////////////////////
  Text _DllFile(Real forceRelease)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text dllPath.rel = GetAbsolutePath(
      TolPackage::TolPlatform::ObtainDllFolder(?))+"/";
    dllPath.rel+Name(_this)+"."+GetSharedLibExt(0)
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Text DllFile(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    Text try = _DllFile(false);
    If(OSFilExist(try), try, _DllFile(true))
  };
  
  ////////////////////////////////////////////////////////////////////////////
  Real StartActions(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
    If(_is_started, False, {
      Real _is_started := True;
      Text _dllFile := DllFile(?);
      NameBlock CppTools := LoadDynLib(_dllFile);
      True
    })
  }
]];
//////////////////////////////////////////////////////////////////////////////
