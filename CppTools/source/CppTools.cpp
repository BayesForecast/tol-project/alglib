/* CppTools.cpp: AlgLib API (http://www.alglib.net/)

   Copyright (C) 2005-2011, Bayes Decision, SL (Spain [EU])

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
   USA.
 */


//Starts local namebock scope
#define LOCAL_NAMEBLOCK _local_namebtntLock_

#if defined(_MSC_VER)
#  include <win_tolinc.h>
#endif

#include <tol/tol_LoadDynLib.h>
#include <tol/tol_bcommon.h>
#include <tol/tol_bnameblock.h>
#include <tol/tol_bdatgra.h>
#include <tol/tol_btxtgra.h>
#include <tol/tol_bmatgra.h>
#include <interpolation.h>

using namespace alglib;

#define dMat(arg) ((DMat&)Mat(arg))
#define b2dMat(M) ((DMat&)(M))

//Creates local namebtntLock container
static BUserNameBlock* _local_unb_ = NewUserNameBlock();

//Creates the reference to local namebtntLock
static BNameBlock& _local_namebtntLock_ = _local_unb_->Contens();

//Entry point of library returns the NameBlock to LoadDynLib
//This is the only one exported function 
DynAPI void* GetDynLibNameBlockAlgLib()
{
  BUserNameBlock* copy = NewUserNameBlock();
  copy->Contens() = _local_unb_->Contens();
  return(copy);
}

#define ERR(cond,msg,ret) \
if(showErr && cond) { \
  Error(_MID<<msg); \
  return(ret); \
}

//--------------------------------------------------------------------
DeclareContensClass(BTxt, BTxtTemporary, 
  BTxtInterp_RBF_QNN_serialize);
DefMethod(1, BTxtInterp_RBF_QNN_serialize, 
  "Interp.RBF.QNN.serialize", 2, 2, "Matrix Real",
  "(Matrix XY, Real d)",
  "RBF Radial basis function Interpolation http://www.alglib.net/interpolation/introductiontorbfs.php"
  "Builds a RBF-QNN d-dimensional vectorial interpolation model and serializes it as a string.\n"
  "Each row in XY has d+n columns representing a d-ddimensional point interpolation "
  "and the values of a n-dimensional vector of functions.\n"
  "Columns 1 to d of XY have the d coordinates of independent variables.\n"
  "Columns d to d+n of XY have the values of the n functions to be interpolated.\n",
  BOperClassify::MatrixAlgebra_);
void BTxtInterp_RBF_QNN_serialize::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[AlgLib::CppTools::Interp.RBF.QNN.serialize] ";
  DMat& XY = (DMat&)Mat(Arg(1));
  int d = (int)Real(Arg(2));
  int m = XY.Rows();
  int n = XY.Columns()-d;
  if(!m || !n) 
  { 
    Warning(_MID+"Cannot operate over a ("+m+"x"+n+") matrix");
    return; 
  }
  std::string s;
  rbfmodel model0;
  real_2d_array xy;
  xy.setcontent(m, n+d, XY.Data().Buffer() );
  rbfreport rep;
  // model initialization
  rbfcreate(d, n, model0);
  rbfsetpoints(model0, xy);
  rbfsetalgoqnn(model0);
  rbfbuildmodel(model0, rep);
  // Serialization 
  alglib::rbfserialize(model0, s);
  contens_.Copy(s.c_str(),s.length());
}

//--------------------------------------------------------------------
DeclareContensClass(BMat, BMatTemporary, 
  BMatInterp_RBF_calc);
DefMethod(1, BMatInterp_RBF_calc,
  "Interp.RBF.calc", 2, 2, "Text Matrix",
  "(Text serializedModel, Matrix x)",
  "RBF Radial basis function Interpolation http://www.alglib.net/interpolation/introductiontorbfs.php"
  "Evaluates a serialized RBF n-vectorial model on a d-dimensional set of points x",
  BOperClassify::MatrixAlgebra_);
void BMatInterp_RBF_calc::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[AlgLib::CppTools::Interp.RBF.calc] ";
  BText& modelTxt = Text(Arg(1));
  DMat& X = (DMat&)Mat(Arg(2));
  rbfmodel model1;
  real_1d_array x,y;
  std::string* s = new std::string(modelTxt.String(),modelTxt.Length());
  rbfunserialize(*s, model1);
  delete s;
  int n = model1.c_ptr()->ny;
  int i, m = X.Rows(), d = X.Columns();
  int nx = (int)model1.c_ptr()->nx;
  if(model1.c_ptr()->nx!=d)
  {
    Error(_MID+"Expected dimension was "<<nx+" instead of "<<d);
  }
  const double* X0 = X.Data().Buffer();
  BDat* Y0 = NULL;
  contens_.Alloc(m,n);
  for(i=0; i<m; i++)
  {
    if(!(n==1 && (d==2 || d==3)))
    {
      x.setcontent(d,X0+i*d);
      rbfcalc(model1,x,y);
      double * ypp = y.getcontent();
      if(i==0)
      {
        Y0 = contens_.GetData().GetBuffer();
      }
      memcpy(Y0+i*n,ypp,n*sizeof(double));
    }
    else if(d==2)
    {
      Y0[i] = rbfcalc2(model1,X(i,0),X(i,1));
    }
    else if(d==3)
    {
      Y0[i] = rbfcalc3(model1,X(i,0),X(i,1),X(i,2));
    }
  }
}


//--------------------------------------------------------------------
DeclareContensClass(BMat, BMatTemporary, 
  BMatInterp_RBF_2D_gridcalc);
DefMethod(1, BMatInterp_RBF_2D_gridcalc,
  "Interp.RBF.2D.gridcalc", 3, 3, "Text Matrix Matrix",
  "(Text serializedModel, Matrix x, Matrix y)",
  "RBF Radial basis function Interpolation http://www.alglib.net/interpolation/introductiontorbfs.php"
  "Evaluates a serialized RBF 2D model on a 2D grid",
  BOperClassify::MatrixAlgebra_);
void BMatInterp_RBF_2D_gridcalc::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[AlgLib::CppTools::Interp.RBF.2D.gridcalc] ";
  BText& modelTxt = Text(Arg(1));
  DMat& X = (DMat&)Mat(Arg(2));
  DMat& Y = (DMat&)Mat(Arg(3));
  rbfmodel model1;
  real_1d_array x,y;
  real_2d_array z;
  std::string* s = new std::string(modelTxt.String(),modelTxt.Length());
  rbfunserialize(*s, model1);
  delete s;
  int nx = model1.c_ptr()->nx;
  int ny = model1.c_ptr()->ny;
  if(nx!=2)
  {
    Error(_MID+"Cannot apply to non bidimensional model ("+nx+"x"<<ny+")");
    return;
  }
  if(ny!=1)
  {
    Error(_MID+"Cannot apply to non scalar model ("+nx+"x"+ny+")");
    return;
  }
  int i, m = X.Rows(), n = Y.Rows();
  contens_.Alloc(m,n);
  x.setcontent(m,X.Data().Buffer());
  y.setcontent(n,Y.Data().Buffer());
  rbfgridcalc2(model1,x,m,y,n,z);
  double ** zpp = z.c_ptr()->ptr.pp_double;
  BDat* Z = contens_.GetData().GetBuffer();
  for(i=0; i<m; i++)
  {
    memcpy(Z+i*n,zpp[i],n*sizeof(double));
  }
}


//--------------------------------------------------------------------
DeclareContensClass(BMat, BMatTemporary, BMat_rmatrixevd_eigen);
DefMethod(1, BMat_rmatrixevd_eigen,
  "rmatrixevd.eigen", 1, 1, "Matrix",
  "(Matrix x)",
  "Returns eigenvalues of a real matrix "
  "http://www.alglib.net/eigen/nonsymmetric/nonsymmetricevd.php",
  BOperClassify::MatrixAlgebra_);
void BMat_rmatrixevd_eigen::CalcContens()
//--------------------------------------------------------------------
{
  static BText _MID = "[AlgLib::CppTools::rmatrixevd.eigen] ";
  DMat& A = (DMat&)Mat(Arg(1));
  int i;
  int r = A.Rows();
  int c = A.Columns();
  if(r!=c) 
  { 
    Error(_MID+"Only square matrix are allowed.");
    return;
  }
  real_2d_array a; 
  ae_int_t n = r;
  ae_int_t vneeded=0;
  real_1d_array wr;
  real_1d_array wi;
  real_2d_array vl;
  real_2d_array vr;
  a.setcontent(r, c, A.Data().Buffer() );
  bool ok = rmatrixevd(a,n,vneeded,wr,wi,vl,vr);
  if(r!=c) 
  { 
    Error(_MID+"Algorithm doesn't converge!.");
    return;
  }  
  contens_.Alloc(r,2);
  for(i=0; i<r; i++)
  {
    contens_(i,0) = wr[i];
    contens_(i,1) = wi[i];
  }
}
