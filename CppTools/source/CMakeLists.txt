add_subdirectory( alglib )

include_directories(${TOL_INCLUDE_DIR} ${ALGLIB_INCLUDE_DIR})

add_tol_package( AlgLib CppTools.cpp )

target_link_libraries( AlgLib PRIVATE alglib )